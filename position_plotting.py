# -*- coding: utf-8 -*-
"""
Created by: Zach Gasick

Last updated: 11/21/19
"""

'''
The purpose of this script is to read in data from a simulation, and generate a plot/plots of the data as time progresses
'''

import os
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

def positionPlotPoints(rx, ry, rz, q):
    r = np.reshape(np.array([q[0], q[1], q[2]]), (3,1))
    p = np.reshape(np.array([q[3], q[4], q[5], q[6]]), (4,1))
    
    # Set of all spherical angles:
    u = np.linspace(0, 2 * np.pi, 50)
    v = np.linspace(0, np.pi, 50)
    
    # Cartesian coordinates that correspond to the spherical angles:
    # (this is the equation of an ellipsoid):
    x = rx * np.outer(np.cos(u), np.sin(v))
    y = ry * np.outer(np.sin(u), np.sin(v))
    z = rz * np.outer(np.ones_like(u), np.cos(v))
    
    for i in range(len(x)):
        for j in range(len(x[i])):
            sb = np.reshape(np.array([x[i][j], y[i][j], z[i][j]]), (3, 1))
            s = r + np.dot(pToA(p), sb)
            x[i][j] = s[0][0]
            y[i][j] = s[1][0]
            z[i][j] = s[2][0]
        
    return x,y,z

def pToA(e):
    #generate orientation matrix from 4 euler parameters (e = (4,1))
    
    e0 = e[0]
    ep = e[1:4]
    return (2*e0**2 - 1)*np.identity(3) + 2*(np.dot(ep,np.reshape(ep, (1,3))) + e0*tilde(ep))

def tilde(v):
    #tilde operator
    #takes a 3x1 vector
    
    return np.array([[0, -1*v[2][0], v[1][0]], [v[2][0], 0, -1*v[0][0]], [-1*v[1][0], v[0][0], 0]])

def main(timeSlice):
    #####-----#####
    #Parse the inputs
    #####-----#####
    
    datFiles = glob.glob('*.dat')
    datFiles.sort(key=os.path.getmtime)
    datFile = datFiles[0]
    
    with open (datFile, 'r') as f:
        lines = f.readlines()
    
    #first line is just a header
    lineInd = 1
    
    ellipsoids = []     #collection of [radx, rady, radz]
    posDat = []         #collection of [el1 q, el2 q, el3 q, ...]
    time = []
    
    while lines[lineInd] != 'Time Steps\n':
        elLine = lines[lineInd].replace('\n', '').split(' ')
        ellip = [float(elLine[0]), float(elLine[1]), float(elLine[2])]
        
        ellipsoids.append(ellip)
        lineInd += 1
    
    lineInd += 1
    
    posStep = []
    while lineInd < len(lines):
        line = lines[lineInd]
        if 'time' in line:
            nt = float(line.replace('\n', '').split('=')[1])
            time.append(nt)
            if nt != 0:
                posDat.append(posStep)
                posStep = []
        else:
            newq = []
            line = line.replace('\n', '').split(' ')
            for el in range(7):
                newq.append(float(line[el]))
            posStep.append(newq)
        lineInd += 1
    posDat.append(posStep)
    ##plotting goes here
    
    maxdim = 0
    for el in ellipsoids:
        for r in el:
            if r > maxdim:
                maxdim = r
    
    maxpos = 0
    for t in posDat:
        for el in t:
            for i in range(3):
                if el[i] > maxpos:
                    maxpos = el[i]
    
    graphAxis = maxdim + maxpos
    
    slicesToPlot = []
    if timeSlice == 'all':
        for i in range(len(time)):
            slicesToPlot.append(i)
    else:
        times = timeSlice.replace('\n', '').split(',')
        for timestr in times:
            if '-' in timestr:
                ts = timestr.split('-')
                startt = int(ts[0])
                endt = int(ts[1])
                for i in range(startt, endt + 1):
                    slicesToPlot.append(i)
            else:
                slicesToPlot.append(int(timestr))
    
    for time in slicesToPlot:
        fig = plt.figure(figsize=plt.figaspect(1))  # Square figure
        ax = fig.add_subplot(111, projection='3d')
        
        for i in range(len(ellipsoids)):
            el = ellipsoids[i]
            x, y, z = positionPlotPoints(el[0], el[1], el[2], posDat[time][i])
            # Plot:
            ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='b')
        
        for axis in 'xyz':
            getattr(ax, 'set_{}lim'.format(axis))((-graphAxis, graphAxis))
        
        fig.set_size_inches(12, 12, forward=True)
        plt.show()
    
if __name__ == '__main__':
    #valid entries: 'all', '0,1,2,...', '0-3, 7-12,...' etc...
    #   no spaces, only special characters allowed are ',' and '-'
    timeSlice = 'all'
    main(timeSlice)