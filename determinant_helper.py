# -*- coding: utf-8 -*-
'''
Created by: Zach Gasick

Last updated: 12/1/19
'''

import re

def determinant(M):
    det = 0
    lenM = len(M)
    if lenM == 2:
        det += M[0][0]*M[1][1] - M[1][0]*M[0][1]
        
    else:
        for i in range(lenM):
            newm = []
            for j in range(lenM):
                row = []
                for k in range(lenM):
                    if j != 0 and k != i:
                        row.append(M[j][k])
                if len(row) > 1:
                    newm.append(row)
            if i%2 == 0:
                det += M[0][i]*determinant(newm)
            else:
                det -= M[0][i]*determinant(newm)
    return det

def strdet(M):
    strd = ''
    lenM = len(M)
    if lenM == 2:
        strd += M[0][0] + '*' + M[1][1] + '-' + M[1][0] + '*' + M[0][1]
        
    else:
        for i in range(lenM):
            newm = []
            for j in range(lenM):
                row = []
                for k in range(lenM):
                    if j != 0 and k != i:
                        row.append(M[j][k])
                if len(row) > 1:
                    newm.append(row)
            if i == 0:
                strd += M[0][i] + '*[' + strdet(newm) + ']'
            elif i%2 == 0:
                strd += '+' + M[0][i] + '*[' + strdet(newm) + ']'
            else:
                strd += '-' + M[0][i] + '*[' + strdet(newm) + ']'
    return strd

def multpolystr(poly1, poly2, var):
    #'a0*x+a1' and 'a0*x^2+a1*x+a2'
    rstr = ''
    poly1 = poly1.replace('+', ' ').replace('-', ' -')
    poly2 = poly2.replace('+', ' ').replace('-', ' -')
    
    p1 = [el for el in poly1.split(' ') if el != '']
    p2 = [el for el in poly2.split(' ') if el != '']
    
    lp1 = len(p1)
    lp2 = len(p2)
    
    e1 = [0]*lp1
    e2 = [0]*len(p2)
    for i in range(lp1):
        if var in p1[i]:
            if '^' not in p1[i]:
                e1[i] = 1
            else:
                e1[i] = int(p1[i].split('^')[1])
        if var in p2[i]:
            if '^' not in p2[i]:
                e2[i] = 1
            else:
                e2[i] = int(p2[i].split('^')[1])
    
    #sorting from highest to lowest
    for i in range(lp1 - 1):
        for j in range(i, lp1 - 1):
            if e1[i] < e1[i + 1]:
                t = e1[i]
                e1[i] = e1[i + 1]
                e1[i + 1] = t
                
                t = p1[i]
                p1[i] = p1[i + 1]
                p1[i + 1] = t
    
    for i in range(lp2 - 1):
        for j in range(i, lp2 - 1):
            if e2[i] < e2[i + 1]:
                t = e2[i]
                e2[i] = e2[i + 1]
                e2[i + 1] = t
                
                t = p2[i]
                p2[i] = p2[i + 1]
                p2[i + 1] = t
    
    print(p1)
    print(p2)
    
    rpoly = []
    for a in p1:
        for b in p2:
            nstr = ''
            ex = 0
            pos = True
            
            #find sign
            if '-' in a and '-' in b:
                a = a.replace('-', '')
                b = b.replace('-', '')
            elif '-' in a or '-' in b:
                a = a.replace('-', '')
                b = b.replace('-', '')
                pos = False
            else:
                pass
            print(pos)
            #find exponent
            newa = [a]
            newb = [b]
            if var in a and '^' in a:
                newa = a.split('^')
                newa[0] = newa[0].replace('*' + var, '')
                ex += int(newa[1])
            elif var in a and '^' not in a:
                ex += 1
                newa[0] = newa[0].replace('*' + var, '')
            else:
                pass
            if var in b and '^' in b:
                newb = b.split('^')
                newb[0] = newb[0].replace('*' + var, '')
                ex += int(newb[1])
            elif var in b and '^' not in b:
                ex += 1
                newb[0] = newb[0].replace('*' + var, '')
            else:
                pass
            
            #assemble the components
            if pos:
                nstr += '+'
            else:
                nstr += '-'
            
            nstr += newa[0] + '*' + newb[0]
            
            if ex == 0:
                pass
            elif ex == 1:
                nstr += '*' + var
            else:
                nstr += '*' + var + '^' + str(ex)
            
            cstr = list(nstr)
            if cstr[0] == '+':
                cstr.pop(0)
                nstr = ''.join(cstr)
            
            rpoly.append(nstr)
    return nstr

if __name__ == '__main__':
    A = [['a00', 'a01', 'a02', 'a03'],
         ['a10', 'a11', 'a12', 'a13'],
         ['a20', 'a21', 'a22', 'a23'],
         ['a30', 'a31', 'a32', 'a33']]
    
    
    B = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]]
    #print(determinant(B))
    dstr = strdet(A)
    print(dstr)
    print()
    print('-'*30)
    print()
    polynomial1 = 'a3*x^3+a1*x-a0'
    polynomial2 = '-b0+b2*x^2-b1*x'
    print(multpolystr(polynomial1, polynomial2, 'x'))