/*
Created by: Zach Gasick

Last updated: 12/12/19
*/

#include <math.h>
#include <stdio.h>
#include <vector>
#include "structures.h"
#include "matrix_ops.h"

void broadPhaseDetection(vector<Ellipsoid>) {
	
}

int broadPhasePair(Ellipsoid e1, Ellipsoid e2) {
	//0 if no contact, 1 if contact


	//initialize Q1 and Q2
	vector<vector<float>> Q1m{ { 0, 0, 0, 0 }, {0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } }, Q2m{ { 0, 0, 0, 0 }, {0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

	//set up vectors for Q
	vector<vector<float>> A = pToA(e1.p);
	vector<vector<float>> Rinv = { {1 / e1.xrad, 0, 0 }, {0, 1 / e1.yrad, 0 }, { 0, 0, 1 / e1.zrad } };
	//tr = A*Rinv*Rinv*Atrans
	vector<vector<float>> tr = mmMult(mmMult(mmMult(A, Rinv, 3, 3, 3), Rinv, 3, 3, 3), transpose(A, 3, 3), 3, 3, 3);
	vector<vector<float>> test = mmMult(A, Rinv, 3, 3, 3);
	//side = -btrans*A*Rinv*Rinv*Atrans
	vector<float> side = svMult(vmMult(vmMult(vmMult(vmMult(e1.r, A, 3, 3), Rinv, 3, 3), Rinv, 3, 3), transpose(A, 3, 3), 3, 3), -1., 3);
	//b1 = -btrans*A*Rinv*Rinv*Atrans*b - 1
	float bl = -1*vvSMult(vmMult(vmMult(vmMult(vmMult(e1.r, A, 3, 3), Rinv, 3, 3), Rinv, 3, 3), transpose(A, 3, 3), 3, 3), e1.r, 3) - 1.;

	Q1m[0][0] = tr[0][0], Q1m[0][1] = tr[0][1], Q1m[0][2] = tr[0][2], Q1m[0][3] = side[0];
	Q1m[1][0] = tr[1][0], Q1m[1][1] = tr[1][1], Q1m[1][2] = tr[1][2], Q1m[1][3] = side[1];
	Q1m[2][0] = tr[2][0], Q1m[2][1] = tr[2][1], Q1m[2][2] = tr[2][2], Q1m[2][3] = side[2];
	Q1m[3][0] = side[0],  Q1m[3][1] = side[1],  Q1m[3][2] = side[2],  Q1m[3][3] = bl;
	for (int ii = 0; ii < 4; ii++) {
		for (int jj = 0; jj < 4; jj++) {
			printf("%.2f ", Q1m[ii][jj]);
		}
		printf("\n");
	}

	//Now fill in Q2:
	A = pToA(e2.p);
	Rinv[0][0] = 1 / e2.xrad, Rinv[1][1] = 1 / e2.yrad, Rinv[2][2] = 1 / e2.zrad;
	tr = mmMult(mmMult(mmMult(A, Rinv, 3, 3, 3), Rinv, 3, 3, 3), transpose(A, 3, 3), 3, 3, 3);
	side = svMult(vmMult(vmMult(vmMult(vmMult(e1.r, A, 3, 3), Rinv, 3, 3), Rinv, 3, 3), transpose(A, 3, 3), 3, 3), -1., 3);
	bl = -1 * vvSMult(vmMult(vmMult(vmMult(vmMult(e1.r, A, 3, 3), Rinv, 3, 3), Rinv, 3, 3), transpose(A, 3, 3), 3, 3), e1.r, 3) - 1.;

	Q2m[0][0] = tr[0][0], Q2m[0][1] = tr[0][1], Q2m[0][2] = tr[0][2], Q2m[0][3] = side[0];
	Q2m[1][0] = tr[1][0], Q2m[1][1] = tr[1][1], Q2m[1][2] = tr[1][2], Q2m[1][3] = side[1];
	Q2m[2][0] = tr[2][0], Q2m[2][1] = tr[2][1], Q2m[2][2] = tr[2][2], Q2m[2][3] = side[2];
	Q2m[3][0] = side[0],  Q2m[3][1] = side[1],  Q2m[3][2] = side[2],  Q2m[3][3] = bl;
	
	//calculate coefficients
	//Script provided by Wei Hu
	/*
	Note: my method was to take the determinant of Q1 and Q2 for b4 and b0
		then, for b3, start with Q1, then swap 1 row and take det, then sum
		same for b1, except starting with Q2
		then, take all combinations of 2 rows Q1 and 2 rows Q2 for b2
	THIS METHOD FAILED ON B1 AND B3 - going with Wei's code
	*/

	// charac = b4*x^4 + b3*x^3 + b2*x^2 + b1*x + b0
	// 4*a3*x^3 + 3*a2*x^2 + 2*a1*x + a0
	// where:

	float Q1[10] = { Q1m[0][0], Q1m[0][1], Q1m[0][2], Q1m[0][3], Q1m[1][1], Q1m[1][2], Q1m[1][3], Q1m[2][2], Q1m[2][3], Q1m[3][3] };
	float Q2[10] = { Q2m[0][0], Q2m[0][1], Q2m[0][2], Q2m[0][3], Q2m[1][1], Q2m[1][2], Q2m[1][3], Q2m[2][2], Q2m[2][3], Q2m[3][3] };
	
	float b0 = 2 * Q2[2] * Q2[4] * Q2[8] * Q2[3] - Q2[3] * Q2[3] * Q2[4] * Q2[7] - Q2[0] * Q2[5] * Q2[5] * Q2[9] - 2 * Q2[1] * Q2[6] * Q2[2] * Q2[8] + Q2[3] * Q2[3] * Q2[5] * Q2[5]
		- 2 * Q2[2] * Q2[6] * Q2[5] * Q2[3] + Q2[1] * Q2[1] * Q2[8] * Q2[8] + 2 * Q2[1] * Q2[5] * Q2[2] * Q2[9] - 2 * Q2[1] * Q2[5] * Q2[8] * Q2[3] - Q2[0] * Q2[6] * Q2[6] * Q2[7] 
		- Q2[0] * Q2[4] * Q2[8] * Q2[8] - Q2[1] * Q2[1] * Q2[7] * Q2[9] - Q2[2] * Q2[2] * Q2[4] * Q2[9] + Q2[2] * Q2[2] * Q2[6] * Q2[6] + Q2[0] * Q2[4] * Q2[7] * Q2[9] 
		+ 2 * Q2[0] * Q2[5] * Q2[8] * Q2[6] + 2 * Q2[1] * Q2[6] * Q2[7] * Q2[3];
	
	float b1 = -2 * Q2[0] * Q1[6] * Q2[7] * Q2[6] + Q1[0] * Q2[4] * Q2[7] * Q2[9] - Q1[0] * Q2[4] * Q2[8] * Q2[8] - Q1[0] * Q2[5] * Q2[5] * Q2[9] - Q1[0] * Q2[7] * Q2[6] * Q2[6] 
		- Q2[0] * Q1[7] * Q2[6] * Q2[6] - Q2[0] * Q1[4] * Q2[8] * Q2[8] - Q2[0] * Q2[5] * Q2[5] * Q1[9] + 2 * Q1[1] * Q2[1] * Q2[8] * Q2[8] + 2 * Q2[1] * Q2[1] * Q1[8] * Q2[8] 
		- Q2[1] * Q2[1] * Q1[7] * Q2[9] - Q2[1] * Q2[1] * Q2[7] * Q1[9] + 2 * Q1[2] * Q2[2] * Q2[6] * Q2[6] - Q1[4] * Q2[2] * Q2[2] * Q2[9] - Q2[4] * Q2[2] * Q2[2] * Q1[9] 
		+ 2 * Q1[6] * Q2[2] * Q2[2] * Q2[6] + 2 * Q1[3] * Q2[5] * Q2[5] * Q2[3] - Q1[4] * Q2[7] * Q2[3] * Q2[3] - Q2[4] * Q1[7] * Q2[3] * Q2[3] + 2 * Q1[5] * Q2[5] * Q2[3] * Q2[3] 
		+ 2 * Q1[0] * Q2[5] * Q2[8] * Q2[6] + 2 * Q2[0] * Q2[5] * Q2[8] * Q1[6] + Q2[0] * Q1[4] * Q2[7] * Q2[9] + Q2[0] * Q2[4] * Q2[7] * Q1[9] + Q2[0] * Q2[4] * Q1[7] * Q2[9] 
		- 2 * Q2[0] * Q2[4] * Q1[8] * Q2[8] + 2 * Q2[0] * Q1[5] * Q2[8] * Q2[6] - 2 * Q2[0] * Q1[5] * Q2[5] * Q2[9] + 2 * Q2[0] * Q2[5] * Q1[8] * Q2[6] 
		- 2 * Q1[1] * Q2[1] * Q2[7] * Q2[9] - 2 * Q1[1] * Q2[5] * Q2[8] * Q2[3] - 2 * Q1[1] * Q2[6] * Q2[2] * Q2[8] + 2 * Q1[1] * Q2[5] * Q2[2] * Q2[9] 
		+ 2 * Q1[1] * Q2[6] * Q2[7] * Q2[3] - 2 * Q2[1] * Q1[6] * Q2[2] * Q2[8] + 2 * Q2[1] * Q2[5] * Q1[2] * Q2[9] + 2 * Q2[1] * Q1[6] * Q2[7] * Q2[3] 
		- 2 * Q2[1] * Q2[6] * Q1[2] * Q2[8] - 2 * Q2[1] * Q2[5] * Q1[8] * Q2[3] + 2 * Q2[1] * Q1[5] * Q2[2] * Q2[9] - 2 * Q2[1] * Q2[6] * Q2[2] * Q1[8] 
		+ 2 * Q2[1] * Q2[6] * Q1[7] * Q2[3] + 2 * Q2[1] * Q2[6] * Q2[7] * Q1[3] - 2 * Q2[1] * Q1[5] * Q2[8] * Q2[3] - 2 * Q2[1] * Q2[5] * Q2[8] * Q1[3] 
		+ 2 * Q2[1] * Q2[5] * Q2[2] * Q1[9] - 2 * Q1[2] * Q2[4] * Q2[2] * Q2[9] - 2 * Q1[2] * Q2[6] * Q2[5] * Q2[3] + 2 * Q1[2] * Q2[4] * Q2[8] * Q2[3] 
		+ 2 * Q2[2] * Q1[4] * Q2[8] * Q2[3] + 2 * Q2[2] * Q2[4] * Q1[8] * Q2[3] + 2 * Q2[2] * Q2[4] * Q2[8] * Q1[3] - 2 * Q2[2] * Q1[6] * Q2[5] * Q2[3] 
		- 2 * Q2[2] * Q2[6] * Q1[5] * Q2[3] - 2 * Q2[2] * Q2[6] * Q2[5] * Q1[3] - 2 * Q1[3] * Q2[4] * Q2[7] * Q2[3];
	
	float b2 = Q1[6] * Q1[6] * Q2[2] * Q2[2] + Q1[2] * Q1[2] * Q2[6] * Q2[6] + Q1[1] * Q1[1] * Q2[8] * Q2[8] + Q2[1] * Q2[1] * Q1[8] * Q1[8] + Q1[3] * Q1[3] * Q2[5] * Q2[5] 
		+ Q1[5] * Q1[5] * Q2[3] * Q2[3] + 0.2e1 * Q1[0] * Q2[5] * Q2[8] * Q1[6] + Q1[0] * Q2[4] * Q2[7] * Q1[9] + Q1[0] * Q2[4] * Q1[7] * Q2[9] 
		- 0.2e1 * Q1[0] * Q2[4] * Q1[8] * Q2[8] + 0.2e1 * Q1[0] * Q1[5] * Q2[8] * Q2[6] - 0.2e1 * Q1[0] * Q1[5] * Q2[5] * Q2[9] + 0.2e1 * Q1[0] * Q2[5] * Q1[8] * Q2[6] 
		- Q1[0] * Q1[7] * Q2[6] * Q2[6] - Q1[0] * Q1[4] * Q2[8] * Q2[8] - Q1[0] * Q2[5] * Q2[5] * Q1[9] - Q2[0] * Q1[5] * Q1[5] * Q2[9] - Q2[0] * Q2[4] * Q1[8] * Q1[8] 
		- Q2[0] * Q1[6] * Q1[6] * Q2[7] - Q1[1] * Q1[1] * Q2[7] * Q2[9] - Q2[1] * Q2[1] * Q1[7] * Q1[9] - Q1[2] * Q1[2] * Q2[4] * Q2[9] - Q1[4] * Q2[2] * Q2[2] * Q1[9] 
		- Q1[3] * Q1[3] * Q2[4] * Q2[7] - Q1[4] * Q1[7] * Q2[3] * Q2[3] + Q2[0] * Q1[4] * Q2[7] * Q1[9] + Q2[0] * Q1[4] * Q1[7] * Q2[9] + Q2[0] * Q2[4] * Q1[7] * Q1[9] 
		- 0.2e1 * Q2[0] * Q1[4] * Q1[8] * Q2[8] + 0.2e1 * Q2[0] * Q1[5] * Q1[8] * Q2[6] + 0.2e1 * Q2[0] * Q1[5] * Q2[8] * Q1[6] - 0.2e1 * Q2[0] * Q1[5] * Q2[5] * Q1[9] 
		+ 0.2e1 * Q2[0] * Q2[5] * Q1[8] * Q1[6] - 0.2e1 * Q2[0] * Q1[6] * Q1[7] * Q2[6] - 0.2e1 * Q1[1] * Q1[6] * Q2[2] * Q2[8] + 0.4e1 * Q1[1] * Q2[1] * Q1[8] * Q2[8] 
		- 0.2e1 * Q1[1] * Q2[1] * Q1[7] * Q2[9] - 0.2e1 * Q1[1] * Q2[1] * Q2[7] * Q1[9] + 0.2e1 * Q1[1] * Q2[5] * Q1[2] * Q2[9] + 0.2e1 * Q1[1] * Q1[6] * Q2[7] * Q2[3] 
		- 0.2e1 * Q1[1] * Q2[6] * Q1[2] * Q2[8] - 0.2e1 * Q1[1] * Q2[5] * Q1[8] * Q2[3] + 0.2e1 * Q1[1] * Q1[5] * Q2[2] * Q2[9] - 0.2e1 * Q1[1] * Q2[6] * Q2[2] * Q1[8] 
		+ 0.2e1 * Q1[1] * Q2[6] * Q1[7] * Q2[3] + 0.2e1 * Q1[1] * Q2[6] * Q2[7] * Q1[3] - 0.2e1 * Q1[1] * Q1[5] * Q2[8] * Q2[3] - 0.2e1 * Q1[1] * Q2[5] * Q2[8] * Q1[3] 
		+ 0.2e1 * Q1[1] * Q2[5] * Q2[2] * Q1[9] + 0.2e1 * Q2[1] * Q1[6] * Q1[7] * Q2[3] + 0.2e1 * Q2[1] * Q1[5] * Q2[2] * Q1[9] + 0.2e1 * Q2[1] * Q1[6] * Q2[7] * Q1[3] 
		- 0.2e1 * Q2[1] * Q2[5] * Q1[8] * Q1[3] - 0.2e1 * Q2[1] * Q1[6] * Q1[2] * Q2[8] - 0.2e1 * Q2[1] * Q2[6] * Q1[2] * Q1[8] - 0.2e1 * Q2[1] * Q1[5] * Q1[8] * Q2[3] 
		+ 0.2e1 * Q2[1] * Q1[5] * Q1[2] * Q2[9] + 0.2e1 * Q2[1] * Q2[6] * Q1[7] * Q1[3] - 0.2e1 * Q2[1] * Q1[5] * Q2[8] * Q1[3] + 0.2e1 * Q2[1] * Q2[5] * Q1[2] * Q1[9] 
		- 0.2e1 * Q2[1] * Q1[6] * Q2[2] * Q1[8] + 0.2e1 * Q1[2] * Q1[4] * Q2[8] * Q2[3] - 0.2e1 * Q1[2] * Q1[4] * Q2[2] * Q2[9] + 0.2e1 * Q1[2] * Q2[4] * Q1[8] * Q2[3] 
		+ 0.2e1 * Q1[2] * Q2[4] * Q2[8] * Q1[3] - 0.2e1 * Q1[2] * Q2[4] * Q2[2] * Q1[9] + 0.4e1 * Q1[2] * Q1[6] * Q2[2] * Q2[6] - 0.2e1 * Q1[2] * Q1[6] * Q2[5] * Q2[3] 
		- 0.2e1 * Q1[2] * Q2[6] * Q1[5] * Q2[3] - 0.2e1 * Q1[2] * Q2[6] * Q2[5] * Q1[3] + 0.2e1 * Q2[2] * Q1[4] * Q1[8] * Q2[3] + 0.2e1 * Q2[2] * Q1[4] * Q2[8] * Q1[3] 
		+ 0.2e1 * Q2[2] * Q2[4] * Q1[8] * Q1[3] - 0.2e1 * Q2[2] * Q1[6] * Q1[5] * Q2[3] - 0.2e1 * Q2[2] * Q1[6] * Q2[5] * Q1[3] - 0.2e1 * Q2[2] * Q2[6] * Q1[5] * Q1[3] 
		- 0.2e1 * Q1[3] * Q1[4] * Q2[7] * Q2[3] - 0.2e1 * Q1[3] * Q2[4] * Q1[7] * Q2[3] + 0.4e1 * Q1[3] * Q1[5] * Q2[5] * Q2[3] + Q1[0] * Q1[4] * Q2[7] * Q2[9] 
		- 0.2e1 * Q1[0] * Q1[6] * Q2[7] * Q2[6];

	float b3 = -2 * Q1[0] * Q1[6] * Q1[7] * Q2[6] + Q1[0] * Q1[4] * Q2[7] * Q1[9] + Q1[0] * Q2[4] * Q1[7] * Q1[9] - 2 * Q1[0] * Q1[4] * Q1[8] * Q2[8] + 2 * Q1[0] * Q1[5] * Q1[8] * Q2[6] 
		+ 2 * Q1[0] * Q1[5] * Q2[8] * Q1[6] - 2 * Q1[0] * Q1[5] * Q2[5] * Q1[9] + 2 * Q1[0] * Q2[5] * Q1[8] * Q1[6] - Q1[0] * Q1[5] * Q1[5] * Q2[9] - Q1[0] * Q2[4] * Q1[8] * Q1[8] 
		- Q1[0] * Q1[6] * Q1[6] * Q2[7] - Q2[0] * Q1[4] * Q1[8] * Q1[8] - Q2[0] * Q1[6] * Q1[6] * Q1[7] - Q2[0] * Q1[5] * Q1[5] * Q1[9] + 2 * Q1[1] * Q1[1] * Q1[8] * Q2[8] 
		+ 2 * Q1[1] * Q2[1] * Q1[8] * Q1[8] - Q1[1] * Q1[1] * Q2[7] * Q1[9] - Q1[1] * Q1[1] * Q1[7] * Q2[9] + 2 * Q1[2] * Q1[6] * Q1[6] * Q2[2] - Q1[2] * Q1[2] * Q1[4] * Q2[9] 
		- Q1[2] * Q1[2] * Q2[4] * Q1[9] + 2 * Q1[2] * Q1[2] * Q1[6] * Q2[6] - Q1[3] * Q1[3] * Q1[4] * Q2[7] - Q1[3] * Q1[3] * Q2[4] * Q1[7] + 2 * Q1[3] * Q1[3] * Q1[5] * Q2[5] 
		+ 2 * Q1[3] * Q1[5] * Q1[5] * Q2[3] + Q2[0] * Q1[4] * Q1[7] * Q1[9] + 2 * Q2[0] * Q1[5] * Q1[8] * Q1[6] + 2 * Q1[1] * Q1[6] * Q1[7] * Q2[3] - 2 * Q1[1] * Q2[1] * Q1[7] * Q1[9] 
		+ 2 * Q1[1] * Q1[5] * Q2[2] * Q1[9] + 2 * Q1[1] * Q1[6] * Q2[7] * Q1[3] - 2 * Q1[1] * Q2[5] * Q1[8] * Q1[3] - 2 * Q1[1] * Q1[6] * Q1[2] * Q2[8] 
		- 2 * Q1[1] * Q2[6] * Q1[2] * Q1[8] - 2 * Q1[1] * Q1[5] * Q1[8] * Q2[3] + 2 * Q1[1] * Q1[5] * Q1[2] * Q2[9] + 2 * Q1[1] * Q2[6] * Q1[7] * Q1[3] 
		- 2 * Q1[1] * Q1[5] * Q2[8] * Q1[3] + 2 * Q1[1] * Q2[5] * Q1[2] * Q1[9] - 2 * Q1[1] * Q1[6] * Q2[2] * Q1[8] - 2 * Q2[1] * Q1[5] * Q1[8] * Q1[3] 
		+ 2 * Q2[1] * Q1[5] * Q1[2] * Q1[9] + 2 * Q2[1] * Q1[6] * Q1[7] * Q1[3] - 2 * Q2[1] * Q1[6] * Q1[2] * Q1[8] + 2 * Q1[2] * Q1[4] * Q1[8] * Q2[3] 
		+ 2 * Q1[2] * Q1[4] * Q2[8] * Q1[3] - 2 * Q1[2] * Q1[4] * Q2[2] * Q1[9] + 2 * Q1[2] * Q2[4] * Q1[8] * Q1[3] - 2 * Q1[2] * Q1[6] * Q1[5] * Q2[3] 
		- 2 * Q1[2] * Q1[6] * Q2[5] * Q1[3] - 2 * Q1[2] * Q2[6] * Q1[5] * Q1[3] + 2 * Q2[2] * Q1[4] * Q1[8] * Q1[3] - 2 * Q2[2] * Q1[6] * Q1[5] * Q1[3] 
		- 2 * Q1[3] * Q1[4] * Q1[7] * Q2[3] + Q1[0] * Q1[4] * Q1[7] * Q2[9];

	float b4 = 2 * Q1[2] * Q1[4] * Q1[8] * Q1[3] - Q1[3] * Q1[3] * Q1[4] * Q1[7] - Q1[0] * Q1[5] * Q1[5] * Q1[9] - 2 * Q1[1] * Q1[6] * Q1[2] * Q1[8] + Q1[3] * Q1[3] * Q1[5] * Q1[5] 
		- 2 * Q1[2] * Q1[6] * Q1[5] * Q1[3] + Q1[1] * Q1[1] * Q1[8] * Q1[8] + 2 * Q1[1] * Q1[5] * Q1[2] * Q1[9] - 2 * Q1[1] * Q1[5] * Q1[8] * Q1[3] - Q1[0] * Q1[6] * Q1[6] * Q1[7] 
		- Q1[0] * Q1[4] * Q1[8] * Q1[8] - Q1[1] * Q1[1] * Q1[7] * Q1[9] - Q1[2] * Q1[2] * Q1[4] * Q1[9] + Q1[2] * Q1[2] * Q1[6] * Q1[6] + Q1[0] * Q1[4] * Q1[7] * Q1[9] 
		+ 2 * Q1[0] * Q1[5] * Q1[8] * Q1[6] + 2 * Q1[1] * Q1[6] * Q1[7] * Q1[3];
	
	//determine a components
	float a2 = (b3 / b4) * 3 / 4, a1 = (b2 / b4) / 2, a0 = (b1 / b4) / 4;

	float zeta = -18 * a2 * a1 * a0 + 4 * a2 * a2 * a2 * a0 - a2 * a2 * a1 * a1 + 4 * a1 * a1 * a1 + 27 * a0 * a0;
	
	float c1 = a2 * 2 / 3, c0 = a1 / 3;

	//find roots
	printf("4 coeffs: %.2f, %.2f, %.2f, %.2f, %.2f\n", b4/b4, b3/b4, b2/b4, b1/b4, b0/b4);
	printf("3 coeffs: %.2f, %.2f, %.2f, %.2f\n", 1.00, a2, a1, a0);
	
	//if (c0 > 1) {
	//	return -1;
	//}
	float error = 1, count = 1;
	float vertexQuad = -c1 / 2;
	float second1 = vertexQuad - 1, second2 = vertexQuad + 1;		//zeros of the quadratic
	while ((error > 1 / 1000) && (count < 10)) {
		second1 = second1 - (second1 * second1 + c1 * second1 + c0) / (2 * second1 + c1);
		error = fabsf(second1 * second1 + c1 * second1 + c0);
		count += 1;
	}
	count = 0;
	error = 1;
	while ((error > 1 / 1000) && (count < 10)) {
		second2 = second2 - (second2 * second2 + c1 * second2 + c0) / (2 * second2 + c1);
		error = fabsf(second2 * second2 + c1 * second2 + c0);
		count += 1;
	}
	count = 0;
	//right now second1 and second 2 are the zeros of the derivative of the cubic polynomial
	float lam0 = second1 - 1, lam1 = (second1 + second2) / 2, lam2 = second2 + 1;	//zeros
	error = 1;
	while ((error > 1 / 1000) && (count < 10)) {
		lam0 = lam0 - (lam0 * lam0 * lam0 + a2 * lam0 * lam0 + a1 * lam0 + a0) / (3 * lam0 * lam0 + 2 * a2 * lam0 + a1);
		error = fabsf(lam0 * lam0 * lam0 + a2 * lam0 * lam0 + a1 * lam0 + a0);
		count += 1;
	}
	count = 0;
	error = 1;
	while ((error > 1 / 1000) && (count < 10)) {
		lam1 = lam1 - (lam1 * lam1 * lam1 + a2 * lam1 * lam1 + a1 * lam1 + a0) / (3 * lam1 * lam1 + 2 * a2 * lam1 + a1);
		error = fabsf(lam1 * lam1 * lam1 + a2 * lam1 * lam1 + a1 * lam1 + a0);
		count += 1;
	}
	count = 0;
	error = 1;
	while ((error > 1 / 1000) && (count < 10)) {
		lam2 = lam2 - (lam2 * lam2 * lam2 + a2 * lam2 * lam2 + a1 * lam2 + a0) / (3 * lam2 * lam2 + 2 * a2 * lam2 + a1);
		error = fabsf(lam2 * lam2 * lam2 + a2 * lam2 * lam2 + a1 * lam2 + a0);
		count += 1;
	}
	//right now, lam0, lam1, and lam2 are the roots of the cubic - directly find (numerically) roots of quartic to check roots

	//now we need the roots of the original (two unique positive means no contact, 2 positive means contact)
	//NOTE - WE ONLY NEED THE LAST 2 ROOTS - THEY MUST BE IDENTICAL AND POSITIVE FOR CONTACT
	float root2 = (lam1 + lam2) / 2, root3 = lam2 + 1;
	
	count = 0, error = 1;
	while ((error > 1 / 1000) && (count < 10)) {
		root2 = root2 - (b4 * root2 * root2 * root2 * root2 + b3 * root2 * root2 * root2 + b2 * root2 * root2 + b1 * root2 + b0)
			/ (4 * b4 * root2 * root2 * root2 + 3 * b3 * root2 * root2 + 2 * b2 * root2 + b1);
		error = fabsf(b4 * root2 * root2 * root2 * root2 + b3 * root2 * root2 * root2 + b2 * root2 * root2 + b1 * root2 + b0);
		count += 1;
	}

	count = 0, error = 1;
	while ((error > 1 / 1000) && (count < 10)) {
		root3 = root3 - (b4 * root3 * root3 * root3 * root3 + b3 * root3 * root3 * root3 + b2 * root3 * root3 + b1 * root3 + b0)
			/ (4 * b4 * root3 * root3 * root3 + 3 * b3 * root3 * root3 + 2 * b2 * root3 + b1);
		error = fabsf(b4 * root3 * root3 * root3 * root3 + b3 * root3 * root3 * root3 + b2 * root3 * root3 + b1 * root3 + b0);
		count += 1;
	}

	//root0, 1, 2, 3 are roots of quartic - good to go now
	root2 = round(root2 * 1000000) / 1000000;
	root3 = round(root3 * 1000000) / 1000000;

	printf("important roots: %.2f, %.2f\n", root2, root3);

	if ((root2 < 0) && (root2 == root3)) {
		return 1;
	}
	else {
		return 0;
	}
}

void narrowPhasePair(Ellipsoid e1, Ellipsoid e2) {
	//currently still an error in the broad phase detection
	//want to start on this anyways to at least script a little more

	/*
	Current plan:
	Sensitivity approach: d_new = d_old - [dd/dai dd/daj]^-1 * ?

	when d_new - d_old <<, call it quits
	need to figure out how to get P1, P2 out of this approach
		- I think n will come from c, which will be defined based on ai and aj
		- will need to figure out how to back out ai, aj
	*/

	//set up values needed for later
	float a1 = 0, a2 = 0;
	vector<float> c = { cosf(a1)*cosf(a2), sinf(a1)* cosf(a2), sinf(a2) };
	vector<vector<float>> R1 = { { e1.xrad, 0, 0 }, { 0, e1.yrad, 0 }, { 0, 0, e1.zrad} };
	vector<vector<float>> R2 = { { e2.xrad, 0, 0 }, { 0, e2.yrad, 0 }, { 0, 0, e2.zrad} };
	vector<vector<float>> A1 = pToA(e1.p), A2 = pToA(e2.p);
	vector<float> np1 = mvMult(transpose(A1, 3, 1), c, 3, 3), np2 = mvMult(transpose(A2, 3, 1), svMult(c, -1, 3), 3, 3);
	vector<vector<float>> M1 = mmMult(mmMult(mmMult(A1, R1, 3, 3, 3), R1, 3, 3, 3), transpose(A1, 3, 3), 3, 3, 3);
	vector<vector<float>> M2 = mmMult(mmMult(mmMult(A2, R2, 3, 3, 3), R2, 3, 3, 3), transpose(A2, 3, 3), 3, 3, 3);

	float lam1 = sqrtf(.25 * vvSMult(vmMult(np1, M1, 3, 3), np1, 3)), lam2 = sqrtf(.25 * vvSMult(vmMult(np2, M2, 3, 3), np2, 3));


	//for optimization, use Newton-Raphson
	//	J*deltaA = -g(a), where...
	//		see notes on definition of these quantites - figured out



	//note for later - equation 13 in Dan's paper uses n', not n
	//also: condition 1 for broad phase detection: b0 = det(Q2)/det(Q1)
}