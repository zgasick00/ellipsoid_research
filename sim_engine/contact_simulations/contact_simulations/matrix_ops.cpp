/*
Created by: Zach Gasick

Last updated: 12/12/19
*/

#include <math.h>
#include <numeric>
#include <vector>
#include "matrix_ops.h"

using namespace std;

vector<vector<float>> tilde(vector<float> vec) {
	//initialize matrix
	vector<vector<float>> vt;

	//assign values
	for (int i = 0; i < 3; i++) {
		vt.push_back(vector<float> { 0, 0, 0 });
	}

	vt[0][0] = 0;
	vt[0][1] = -1 * vec[2];
	vt[0][2] = vec[1];

	vt[1][0] = vec[2];
	vt[1][1] = 0;
	vt[1][2] = -1 * vec[0];

	vt[2][0] = -1 * vec[1];
	vt[2][1] = vec[0];
	vt[2][2] = 0;
	
	return vt;
}

vector<vector<float>> E(vector<float> p) {
	//initialize matrix
	vector<vector<float>> Em;

	//assign values
	for (int i = 0; i < 3; i++) {
		Em.push_back(vector<float> { 0, 0, 0, 0 });
	}
	
	Em[0][0] = -1 * p[1];
	Em[0][1] = p[0];
	Em[0][2] = -1 * p[3];
	Em[0][3] = p[2];
	
	Em[1][0] = -1 * p[2];
	Em[1][1] = p[3];
	Em[1][2] = p[0];
	Em[1][3] = -1 * p[1];

	Em[2][0] = -1 * p[3];
	Em[2][1] = -1 * p[2];
	Em[2][2] = p[1];
	Em[2][3] = p[0];
	
	return Em;
}

vector<vector<float>> G(vector<float> p) {
	//initialize matrix
	vector<vector<float>> Gm;

	//assign values
	for (int i = 0; i < 3; i++) {
		Gm.push_back(vector<float> { 0, 0, 0, 0 });
	}

	Gm[0][0] = -1 * p[1];
	Gm[0][1] = p[0];
	Gm[0][2] = p[3];
	Gm[0][3] = -1 * p[2];

	Gm[1][0] = -1 * p[2];
	Gm[1][1] = -1 * p[3];
	Gm[1][2] = p[0];
	Gm[1][3] = p[1];

	Gm[2][0] = -1 * p[3];
	Gm[2][1] = p[2];
	Gm[2][2] = -1 * p[1];
	Gm[2][3] = p[0];

	return Gm;
}

vector<vector<float>> pToA(vector<float> p) {
	//initialize e0, e, identity, and A
	float e0 = p[0];
	vector<float> ep{ p[1], p[2], p[3] };
	vector<vector<float>> A;

	//first term calculations
	float val = 2 * e0 * e0 - 1;
	vector<vector<float>> t1 = I(3);
	t1[0][0] *= val;
	t1[1][1] *= val;
	t1[2][2] *= val;

	//second term
	vector<vector<float>> t2 = smMult((mmAdd(vvLMult(ep, ep, 3), smMult(tilde(ep), e0, 3, 3), 3, 3)), 2, 3, 3);
	//translation: {2 * ([ep * ep.transpose()] + [e0 * tilde(ep)])}
	
	//finish up:
	A = mmAdd(t1, t2, 3, 3);
	return A;
}

vector<float> AToP(vector<vector<float>> A) {
	//initialize p
	vector<float> p(4, 0);
	
	//fill out
	p[0] = sqrtf((A[0][0] + A[1][1] + A[2][2] + 1) / 4);
	p[1] = (A[2][1] - A[1][2]) / (4 * p[0]);
	p[2] = (A[0][2] - A[2][0]) / (4 * p[0]);
	p[3] = (A[1][0] - A[0][1]) / (4 * p[0]);
	
	return p;
}

vector<float> pdToWb(vector<float> p, vector<float> pd) {
	vector<vector<float>> Gm;
	vector<float> wb(3,0);
	Gm = G(p);
	wb = svMult(mvMult(Gm, pd, 3, 4), 2, 3);
	//translation: 2*G*pd
	return wb;
}

vector<float> pdToW(vector<float> p, vector<float> pd) {
	vector<float> ret(3, 0);
	vector<vector<float>> Em = E(p);
	
	//return 2 * E(p) * pd;
	ret = svMult(mvMult(Em, pd, 3, 4), 2, 3);
	
	return ret;
}

vector<float> wbToPd(vector<float> wb, vector<float> p) {
	vector<float> ret(4, 0);
	vector<vector<float>> Gm = G(p);

	//return .5 * G(p).transpose() * wb;
	ret = svMult(mvMult(transpose(Gm, 3, 4), wb, 4, 3), .5, 4);
	return ret;
}

vector<float> wToPd(vector<float> w, vector<float> p) {
	vector<float> ret(4, 0);
	vector<vector<float>> Em = E(p);

	//return .5 * E(p).transpose() * w;
	ret = svMult(mvMult(transpose(Em, 3, 4), w, 4, 3), .5, 4);
	return ret;
}

vector<vector<float>> I(size_t n) {
	vector<vector<float>> Im;
	vector<float> row(n, 0);
	for (int i = 0; i < n; i++) {
		Im.push_back(row);
		Im[i][i] = 1;
	}
	return Im;
}

vector<vector<float>> transpose(vector<vector<float>> arr, size_t row, size_t col) {

	vector<vector<float>> tm;
	vector<float> newRow(row, 0);

	for (int i = 0; i < col; i++) {			//i is the old column, new row
		tm.push_back(newRow);
		for (int j = 0; j < row; j++) {		//j is the old row, new column
			tm[i][j] = arr[j][i];
		}
	}
	return tm;
}

vector<float> vmMult(vector<float> v, vector<vector<float>> m, size_t col1, size_t col2) {
	vector<float> ret(col2, 0);

	for (int i = 0; i < col2; i++) {
		for (int j = 0; j < col1; j++) {
			ret[i] += v[j] * m[j][i];
		}
	}
	return ret;
}

vector<vector<float>> mmMult(vector<vector<float>> m1, vector<vector<float>> m2, size_t row1, size_t col1, size_t col2) {
	vector<vector<float>> ret;
	vector<float> newRow(col2, 0);
	for (int i = 0; i < row1; i++) {
		ret.push_back(newRow);

		for (int j = 0; j < col1; j++) {
			for (int k = 0; k < col2; k++) {
				ret[i][j] += m1[i][k] * m2[k][j];
			}
		}

	}
	return ret;
}

float vvSMult(vector<float> v1, vector<float> v2, size_t n) {
	float ret = 0;
	for (int i = 0; i < n; i++) {
		ret += v1[i] * v2[i];
	}
	return ret;
}

vector<vector<float>> vvLMult(vector<float> v1, vector<float> v2, size_t n) {
	vector<vector<float>> ret;
	vector<float> newRow(n, 0);
	for (int i = 0; i < n; i++) {
		ret.push_back(newRow);
		for (int j = 0; j < n; j++) {
			ret[i][j] = v1[i] * v2[j];
		}
	}

	return ret;
}

vector<float> mvMult(vector<vector<float>> m, vector<float> v, size_t row1, size_t col1) {
	vector<float> ret(row1, 0);
	for (int i = 0; i < row1; i++) {
		for (int j = 0; j < col1; j++) {
			ret[i] += v[j] * m[i][j];
		}
	}
	return ret;
}

vector<float> vvAdd(vector<float> v1, vector<float> v2, size_t n) {
	vector<float> ret(n, 0);
	for (int i = 0; i < n; i++) {
		ret[i] = v1[i] + v2[i];
	}
	return ret;
}

vector<vector<float>> mmAdd(vector<vector<float>> m1, vector<vector<float>> m2, size_t row, size_t col) {
	vector<vector<float>> ret;
	vector<float> newRow(col, 0);
	for (int i = 0; i < row; i++) {
		ret.push_back(newRow);
		for (int j = 0; j < col; j++) {
			ret[i][j] = m1[i][j] + m2[i][j];
		}
	}
	return ret;
}

vector<float> svMult(vector<float> v, float a, size_t n) {
	vector<float> ret(n, 0);
	for (int i = 0; i < n; i++) {
		ret[i] = a * v[i];
	}
	return ret;
}

vector<vector<float>> smMult(vector<vector<float>> m, float a, size_t row, size_t col) {
	vector<vector<float>> ret;
	vector<float> newRow(col, 0);
	for (int i = 0; i < row; i++) {
		ret.push_back(newRow);
		for (int j = 0; j < col; j++) {
			ret[i][j] = m[i][j] * a;
		}
	}
	return ret;
}

float det(vector<vector<float>> M, size_t n) {
	float deter = 0;
	
	if (n == 2) {
		deter += M[0][0] * M[1][1] - M[1][0] * M[0][1];
	}
	else {
		for (int i = 0 ; i < n; i++) {
			vector<vector<float>> newm;
			for (int j = 0 ; j < n; j++) {
				vector<float> row;
				for (int k = 0; k < n; k++) {
					if (j != 0 && k != i) {
						row.push_back(M[j][k]);
					}
					if (row.size() > 1) {
						newm.push_back(row);
					}
				}
			}
			if (i % 2 == 0) {
				deter += M[0][i] * det(newm, n - 1);
			}
			else {
				deter -= M[0][i] * det(newm, n - 1);
			}
		}
	}
	return deter;
}

vector<vector<float>> inverse(vector<vector<float>> M, size_t n) {
	vector<vector<float>> invM;
	float determinant = det(M, n);
	for (int row = 0; row < n; row++) {
		vector<float> Mrow;
		for (int col = 0; col < n; col++) {
			vector<vector<float>> miniM;
			for (int i = 0; i < n - 1; i++) {
				vector<float> newRow;
				for (int j = 0; j < n - 1; j++) {
					if ((i != row) && (j != col)) {
						newRow.push_back(M[i][j]);
					}
				}
				if (newRow.size() > 1) {
					miniM.push_back(newRow);
				}
			}
			if ((row + col) % 2 == 0) {
				Mrow.push_back((1/determinant)*det(miniM, n - 1));
			}
			else {
				Mrow.push_back(-1 * (1/determinant) * det(miniM, n - 1));
			}
		}
		invM.push_back(Mrow);
	}
	invM = transpose(invM, n, n);
	return invM;
}