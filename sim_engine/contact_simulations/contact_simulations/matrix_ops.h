/*
Created by: Zach Gasick:

Last updated: 12/12/19
*/

#ifndef MATRIX_OPS_H
#define MATRIX_OPS_H

#include <vector>

using namespace std;

vector<vector<float>> tilde(vector<float> vec);	//confirmed working

vector<vector<float>> E(vector<float> p);	//confirmed working

vector<vector<float>> G(vector<float> p);	//confirmed working

vector<vector<float>> pToA(vector<float> p);	//confirmed working

vector<float> AToP(vector<vector<float>> A);	//confirmed working

vector<float> pdToWb(vector<float> p, vector<float> pd);	//confirmed working

vector<float> pdToW(vector<float> p, vector<float> pd);		//confirmed working

vector<float> wbToPd(vector<float> wb, vector<float> p);	//confirmed working

vector<float> wToPd(vector<float> w, vector<float> p);		//confirmed working

//standard operations (add, multiply, transpose, inverse (?) )
vector<vector<float>> I(size_t n);	//confirmed working

vector<vector<float>> transpose(vector<vector<float>> arr, size_t row, size_t col);		//confirmed working

vector<float> vmMult(vector<float> v, vector<vector<float>> m, size_t col1, size_t col2);		//confirmed working

vector<vector<float>> mmMult(vector<vector<float>> m1, vector<vector<float>> m2, size_t row1, size_t col1, size_t col2);	//confirmed working

float vvSMult(vector<float> v1, vector<float> v2, size_t n);	//confirmed working

vector<vector<float>> vvLMult(vector<float> v1, vector<float> v2, size_t n);	//confirmed working

vector<float> mvMult(vector<vector<float>> m, vector<float> v, size_t row1, size_t col1);	//confirmed working

vector<float> vvAdd(vector<float> v1, vector<float> v2, size_t n);	//confirmed working

vector<vector<float>> mmAdd(vector<vector<float>> m1, vector<vector<float>> m2, size_t row, size_t col);	//confirmed working

vector<float> svMult(vector<float> v, float a, size_t n);	//confirmed working

vector<vector<float>> smMult(vector<vector<float>> m, float a, size_t row, size_t col);	//confirmed working

float det(vector<vector<float>> m, size_t n);

vector<vector<float>> inverse(vector<vector<float>> M, size_t n);

#endif