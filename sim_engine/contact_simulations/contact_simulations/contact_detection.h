#pragma once

#ifndef CONTACT_DETECTION_H
#define CONTACT_DETECTION_H
/*
Created by: Zach Gasick

Last updated: 12/9/19
*/

#include <vector>
#include "structures.h"

void broadPhaseDetection(vector<Ellipsoid>);

int broadPhasePair(Ellipsoid e1, Ellipsoid e2);

#endif
