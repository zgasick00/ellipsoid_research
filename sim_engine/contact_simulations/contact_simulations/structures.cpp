/*
Created by: Zach Gasick

Last updated: 12/5/19
*/

#include <vector>
#include "structures.h"

using namespace std;

void printEllipsoid(Ellipsoid el) {
	printf("Rx: %.3f -- Ry: %.3f -- Rz: %.3f\n", el.xrad, el.yrad, el.zrad);
	printf("q:   %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n", el.r[0], el.r[1], el.r[2], el.p[0], el.p[1], el.p[2], el.p[3]);
	printf("qd:  %.3f %.3f %.3f %.3f %.3f %.3f\n", el.rd[0], el.rd[1], el.rd[2], el.wb[0], el.wb[1], el.wb[2]);
	printf("qdd: %.3f %.3f %.3f %.3f %.3f %.3f\n", el.rdd[0], el.rdd[1], el.rdd[2], el.wbd[0], el.wbd[1], el.wbd[2]);

}

void printDatabase(Database db) {
	for (int i = 0; i < db.numEllipsoids; i++) {
		printf("Ellipsoid %d:\n", i + 1);
		printEllipsoid(db.simEllipsoids[i]);
	}
	printf("Time of Simulation: 0-%f [s]\n", db.time[db.timesteps - 1]);
	printf("\tNumber of Steps: %d\n", db.timesteps);
}
