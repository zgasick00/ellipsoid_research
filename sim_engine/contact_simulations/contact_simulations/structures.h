#ifndef STRUCTURES_H
#define STRUCTURES_H

/*
Created by: Zach Gasick

Last updated: 12/5/19
*/

#include <vector>

using namespace std;

typedef struct _Ellipsoid {
	int id = -1;   //0 based

	vector<float> r{ 0, 0, 0 }, rd{ 0, 0, 0 }, rdd{ 0, 0, 0 };

	vector<float> p{ 1, 0, 0, 0 }, wb{ 0, 0, 0 }, wbd{ 0, 0, 0 };

	float xrad = 1, yrad = 1, zrad = 1;
}Ellipsoid;

typedef struct _Database {
	int numEllipsoids = 1;
	vector<Ellipsoid> simEllipsoids;

	int timesteps = 1;
	vector<float> time = { -1 };
	vector<vector<vector<float>>> histq;	//[ [[el 1], [el 2], ...], [time 2], [time 3], ... ]
	vector<vector<vector<float>>> histqd;
	vector<vector<vector<float>>> histqdd;
}Database;

void printEllipsoid(Ellipsoid el);

void printDatabase(Database db);

#endif