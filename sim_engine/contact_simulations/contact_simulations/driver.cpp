/*
Created by: Zach Gasick

Last updated: 12/12/19
*/

#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <vector>

#include "structures.h"
#include "matrix_ops.h"
#include "contact_detection.h"

using namespace std;

int main() {
	//Eigen looked in to. Eigen must be included when compiling:
	//g++ my_program.cpp -o my_program
	//view here: https://eigen.tuxfamily.org/dox/GettingStarted.html

	//USER INPUTS BELOW:
	int maxDistance = 3;
	float maxRad = .5;
	float minRad = .3;
	int numEllip = 3;

	float endTime = .1;
	float dt = .01;
	
	//END USER INPUTS

	int steps = (int)round(endTime / dt) + 1;
	Database db;

	vector<float> timeArr;
	timeArr.reserve(steps);
	for (int i = 0; i < steps; i++) {
		timeArr.push_back(round(10000*(dt * i))/10000);
	}
	
	Ellipsoid e1, e2;	//in contact
	e1.id = 0, e1.xrad = 3, e1.yrad = 2, e1.zrad = 5;
	e1.r = { -1, -1.2, 1 }, e1.p = { .64, -.46, -.23, -.57 };

	e2.id = 1, e2.xrad = 1, e2.yrad = 3, e2.zrad = 4;
	e2.r = { -3, 3, 3 }, e2.p = { 1, 0, 0, 0 };

	db.time = timeArr;
	db.timesteps = steps;
	db.simEllipsoids.push_back(e1);
	db.simEllipsoids.push_back(e2);

	int howdwedo;
	howdwedo = broadPhasePair(e1, e2);
	vector<vector<float>> testQ = { {1, 0, 0, 0 }, {0, .25, 0, 0 }, { 0, 0, .11, 0 }, {0, 0, 0, -1} };
	
	printf("Contact state: %d\n", howdwedo);  //should be 1
	printf("Successful Run\n");
	return 0;
}