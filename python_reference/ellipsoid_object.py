# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 16:08:01 2019

Created by: Zach Gasick

Last updated: 10/31/19
"""

import math as m
import numpy as np
import minor_matrix_ops as mm

class Ellipsoid:
    
    def __init__(self, rid, r, p, rd, wb, x, y, z):
        
        self.id = rid   #0 based
        
        self.r = r
        self.p = p
        self.rd = rd
        self.wb = wb
        self.rdd = ''
        self.wbd = ''
        
        self.xrad = x
        self.yrad = y
        self.zrad = z
        
        return
    
    def positionPlotPoints(self):
        rx = self.xrad
        ry = self.yrad
        rz = self.zrad
        
        # Set of all spherical angles:
        u = np.linspace(0, 2 * np.pi, 50)
        v = np.linspace(0, np.pi, 50)
        
        # Cartesian coordinates that correspond to the spherical angles:
        # (this is the equation of an ellipsoid):
        x = rx * np.outer(np.cos(u), np.sin(v))
        y = ry * np.outer(np.sin(u), np.sin(v))
        z = rz * np.outer(np.ones_like(u), np.cos(v))
        
        for i in range(len(x)):
            for j in range(len(x[i])):
                sb = np.reshape(np.array([x[i][j], y[i][j], z[i][j]]), (3, 1))
                s = self.r + np.dot(mm.pToA(self.p), sb)
                x[i][j] = s[0][0]
                y[i][j] = s[1][0]
                z[i][j] = s[2][0]
            
        return x,y,z
    
    def __str__(self):
        retstr = ''
        retstr += 'Ellipsoid ID: ' + str(self.id)
        retstr += '\t-position:'
        for i in range(3):
            retstr +=  ' ' + str(self.r[i])
        for i in range(4):
            retstr += ' ' + str(self.p[i])
        
        retstr += '\n\t-velocity:'
        for i in range(3):
            retstr += ' ' + str(self.rd[i])
        for i in range(3):
            retstr += ' ' + str(self.wb[i])
        
        retstr += '\n\t-acceleration:'
        for i in range(3):
            retstr += ' ' + str(self.rdd[i])
        for i in range(3):
            retstr += ' ' + str(self.wbd[i])
            
        retstr += '''\n\t-x' radius: ''' + str(self.xrad)
        retstr += '''\n\t-y' radius: ''' + str(self.yrad)
        retstr += '''\n\t-z' radius: ''' + str(self.zrad)
        retstr += '\n'
        
        return retstr