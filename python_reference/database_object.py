# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 16:08:01 2019

Created by: Zach Gasick

Last updated: 11/1/19
"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class Database:
    
    def __init__(self):
        self.data = []
        return
    
    def addEllip(self, ellipsoid):
        self.data.append(ellipsoid)
        return
    
    def plotEllips(self, bounding):
        fig = plt.figure(figsize=plt.figaspect(1))  # Square figure
        ax = fig.add_subplot(111, projection='3d')
        
        xx = []
        yy = []
        zz = []
        for ellip in self.data:
            x, y, z = ellip.positionPlotPoints()
            # Plot:
            ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='b')
        
        for pt in range(len(xx)):
            print('{}, {}, {}'.format(xx[pt], yy[pt], zz[pt]))
        
        max_radius = max(bounding[0], bounding[1], bounding[2])
        for axis in 'xyz':
            getattr(ax, 'set_{}lim'.format(axis))((-max_radius, max_radius))
        
        fig.set_size_inches(12, 12, forward=True)
        plt.show()
        
        return
    












if __name__ == '__main__':
    pass