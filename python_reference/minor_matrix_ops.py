# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 16:08:01 2019

Created by: Zach Gasick

Last updated: 10/31/19
"""
import numpy as np
import math as m

def tilde(v):
    #tilde operator
    #takes a 3x1 vector
    
    return np.array([[0, -1*v[2][0], v[1][0]], [v[2][0], 0, -1*v[0][0]], [-1*v[1][0], v[0][0], 0]])

def E(p):
    G = np.array(                   [-1*p[1][0],    p[0][0], -1*p[3][0],    p[2][0]])
    G = np.concatenate((G, np.array([-1*p[2][0],    p[3][0],    p[0][0], -1*p[1][0]])))
    G = np.concatenate((G, np.array([-1*p[3][0], -1*p[2][0],    p[1][0],    p[0][0]])))
    G = np.reshape(G, (3,4))
    return G

def G(p):
    G = np.array(                   [-1*p[1][0],    p[0][0],    p[3][0], -1*p[2][0]])
    G = np.concatenate((G, np.array([-1*p[2][0], -1*p[3][0],    p[0][0],    p[1][0]])))
    G = np.concatenate((G, np.array([-1*p[3][0],    p[2][0], -1*p[1][0],    p[0][0]])))
    G = np.reshape(G, (3,4))
    return G

def pToA(e):
    #generate orientation matrix from 4 euler parameters (e = (4,1))
    
    e0 = e[0]
    ep = e[1:4]
    return (2*e0**2 - 1)*np.identity(3) + 2*(np.dot(ep,np.reshape(ep, (1,3))) + e0*tilde(ep))

def aToP(A):
    e0 = m.sqrt((A[0][0] + A[1][1] + A[2][2] + 1)/4)
    e1 = (A[2][1] - A[1][2])/(4*e0)
    e2 = (A[0][2] - A[2][0])/(4*e0)
    e3 = (A[1][0] - A[0][1])/(4*e0)
    
    return np.reshape(np.array([e0, e1, e2, e3]), (4,1))

def pdTowb(p, pd):
    #lecture 7
    return 2*np.dot(G(p), pd)

def pdTow(p, pd):
    return 2*np.dot(E(p), pd)

def wbToPd(wb, p):
    return .5*np.dot(G(p).T, wb)

def wToPd(w, p):
    return .5*np.dot(E(p).T, w)

def pddToAlphab(p, pdd):
    return 2*np.dot(G(p), pdd)

def pddToAlpha():
    return

def alphabToPdd(p, wb, wbd):
    #lecture 9
    return .5*np.dot(G(p).T, wbd) - .25*np.dot(np.dot(np.reshape(wb, (1,3)), wb), p)

def alphaToPdd():
    return

if __name__ == '__main__':
    pass