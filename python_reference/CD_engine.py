# -*- coding: utf-8 -*-
"""
Created on Fri Nov  1 15:38:46 2019

Created by: Zach Gasick

Last updated: 11/1/19
"""

class DetectionEngine:
    
    def __init__(self, mins, maxs, divisions):
        self.numBins = divisions**3
        self.xs = []
        self.ys = []
        self.zs = []