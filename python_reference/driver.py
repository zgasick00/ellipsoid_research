# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 16:08:01 2019

Created by: Zach Gasick

Last updated: 11/1/19
"""

import numpy as np
import math as m
import ellipsoid_object as eo
import database_object as db
import random as rand

def main(rigids, startLocRange, Rmax, setTest=True):
    data = db.Database()
    for i in range(rigids):
        listq = []
        psum = 0
        for j in range(7):
            if j == 0:
                listq.append(2*startLocRange[0]*(rand.random() - .5))
            elif j == 1:
                listq.append(2*startLocRange[1]*(rand.random() - .5))
            elif j == 2:
                listq.append(2*startLocRange[2]*(rand.random() - .5))
            else:
                p = rand.random()
                psum += p**2
                listq.append(p)
        psum = m.sqrt(psum)
        for j in range(3, 7):
            listq[j] /= psum
        q = np.reshape(np.array(listq), (7, 1))
        
        Rrange = round((1/2)*Rmax, 3)
        Rmin = Rmax - Rrange
        xr = Rmin + Rrange*rand.random()
        yr = Rmin + Rrange*rand.random()
        zr = Rmin + Rrange*rand.random()
        
        if setTest:
            xr = 4
            yr = 2
            zr = 2
            q = np.reshape(np.array([0, 0, 0, 1, 0, 0, 0]), (7,1))
        #temp testing - meant to be no coordinate transformation
        #q[3:8] = np.reshape(np.array([1, 0, 0, 0]), (4, 1))
        
        qd = np.reshape(np.array([0, 0, 0, 0, 0, 0, 0]), (7, 1))
        data.addEllip(eo.Ellipsoid(i, q[0:3], q[3:8], qd[0:3], qd[3:8], xr, yr, zr))
    
    data.plotEllips(startLocRange)




if __name__ == '__main__':
    """
    User Inputs
    """
    rigids = 2
    maxRadius = 3
    startLocRange = [10, 10, 10]
    unitTesting = False
    """
    End User Inputs
    """

    main(rigids, startLocRange, maxRadius, unitTesting)